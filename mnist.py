#!/usr/bin/python3

# Import tensorflow.
import tensorflow as tf

# Import keras.
import keras # [OR] from tensorflow import keras

# Import the MNIST Dataset.
from keras.datasets import mnist

# Load MNIST dataset into respective variables.
(train_imgs, train_labels), (test_imgs, test_labels) = mnist.load_data()

# Training set:
	# train_imgs and train_labels
# Test set:
	# test_imgs and test_labels

# Show 'shape' of train images.
print (str(train_imgs.shape[0]) + " train images @ " + str(train_imgs.shape[1]) + " x " + str(train_imgs.shape[2]) + " pixels and " + str(len(train_labels)) + " labels.")

# Show 'shape' of test images.
print (str(test_imgs.shape[0]) + " test images @ " + str(test_imgs.shape[1]) + " x " + str(test_imgs.shape[2]) + " pixels and " + str(len(test_labels)) + " labels.")

##### Building the network architecture #####
# Import models and layes.
from keras import models, layers

# Create network.
network = models.Sequential()

# Add layers.
network.add(layers.Dense(512, activation='relu', input_shape=(28 * 28,)))
network.add(layers.Dense(10, activation='softmax'))

# Compile network.
network.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])

# Data processing - training set.
train_imgs = train_imgs.reshape((60000, 28 * 28))
train_imgs = train_imgs.astype('float32') / 255.0

# Data processing - test set.
test_imgs = test_imgs.reshape((10000, 28 * 28))
test_imgs = test_imgs.astype('float32') / 255.0

# Import the 'to_categorical' util.
from keras.utils import to_categorical

# Preparing labels.
train_labels = to_categorical(train_labels)
test_labels = to_categorical(test_labels)

# Train network.
network.fit(train_imgs, train_labels, epochs=20, batch_size=128)

# Get results.
tloss, tacc = network.evaluate(test_imgs, test_labels)

print ("Loss: " + str(tloss) + "\nAccuracy: " + str(tacc))



































