#!/bin/bash

#########################################################################
#  Script     : tf_keras_install.sh
#  Author     : BrailinsonDisla
#  Date       : October 5th, 2018
#  Last Edited: October 5th, 2018 @ ~ 12:00, BrailinsonDisla
#########################################################################
# Purpose:
#	The script installs python3 and pip, and the Tensorflow and Keras 
#	frameworks for AI, Machine Learning and Deep Learning.
#
# Requirements:
#	Must be executed with sudo privileges.
#
# Method:
#	--
#
# Syntax:
#	tf_keras_install.sh
#
# Notes:
#	This script only installs tensorflow 1.5 or older versions.
#########################################################################
# Update APT.
sudo apt-get update

# Install Python 3.
sudo apt-get install python3

# Install Pip 3.
sudo apt-get install python3-pip

# Install Tensorflow 1.5 or older.
pip3 install tensorflow==1.5

# Install Keras Library.
pip3 install keras

# Print Done.
echo "Done"

# Wait few seconds and clean screen.
sleep 5; reset
